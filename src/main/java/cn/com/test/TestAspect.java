package cn.com.test;

import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * 类描述
 *
 * @projectName: ECMS2.0
 * @author: panmy
 * @data:2022-03-24 16:37
 **/
@Component
@Aspect
public class TestAspect {


    @Around("execution(* cn.com.test.XXXService.save(..))")
    public void test(){
        System.out.println("around .XXXService.save");
    }
    @Around("execution(* cn.com.test.XXXService.upate(..))")
    public void testaa(){
        System.out.println("around .XXXService.upate");
    }
}
