package cn.com.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * 类描述
 *
 * @projectName: spring-aop-test
 * @author: panmy
 * @data:2022-03-25 11:03
 **/
@SpringBootApplication
@EnableAspectJAutoProxy
public class Appliaction {
    public static void main(String[] args) {
        SpringApplication.run(Appliaction.class);
    }
}
