package cn.com.test;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 类描述
 *
 * @projectName: ECMS2.0
 * @author: panmy
 * @data:2022-03-24 16:40
 **/
@RestController
public class TestController {

    @Resource
    private XXXService xxxService;

    @GetMapping("/test1")
    public void test1(){
        xxxService.save();
    }
    @GetMapping("/test2")
    public void test2(){
        xxxService.upate();
    }
}
